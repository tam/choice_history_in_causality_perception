import pandas as pd
import numpy as np
import os

import bambi as bmb
import arviz as az
import pymc as pm

from typing import Tuple


def standardize(series:pd.Series) -> pd.Series:
    return (series - series.min()) / series.max()

def normalize(group:pd.DataFrame, columns:list) -> pd.DataFrame:
    for col in columns:
        group[col + "_norm"] = (group[col] - group[col].mean()) / group[col].std()
    return group


def countResponses(group:pd.DataFrame) -> pd.DataFrame:
    """Add two columns counting the sequences of same responses for "causal" and "non causal"
    Args:
        group (pd.DataFrame): _description_
    Returns:
        pd.DataFrame: _description_
    """
    c1 = group["causality_judgement1"].values
    c_seq = [1] if c1[0] == 1 else [0]
    nc_seq = [1] if c1[0] == 0 else [0]
    for i in range(len(c1)):
        if i == 0:
            continue
        if c1[i] == 1:
            c_seq.append(c_seq[i-1] + 1)
            nc_seq.append(0)
        else:
            c_seq.append(0)
            nc_seq.append(nc_seq[i-1] + 1)
    group["c_seq"] = c_seq
    group["nc_seq"] = nc_seq
    return group


# "helper functions"
def applyContrasts(data:pd.DataFrame, x:list, y:list, contrast="sum", interaction:str=None) -> tuple:
    """apply contrasts to specified data
    Args:
        data (pd.DataFrame): _description_
        contrast (str, optional): _description_. Defaults to "SUM".
        interaction (str, optional): _description_. Defaults to "None".
    Returns:
        tuple: X, Y
    """
    # "Manual" creation of "Design matrix"
    # define contrasts/ mapping of levels to numbers

    ## Apply contrasts to the two factors'/ groups' levels
    # SUM contrast mapping without interaction
    SumContrX = {
        "non_causal": -1,
        "causal": 1,
        "HC": -1,
        "SZ": 1
    }
    TreatmentContrX = {
        "non_causal": 0,
        "causal": 1,
        "HC": 0,
        "SZ": 1
    }
    if contrast == "sum":
        X = data[x].replace(SumContrX)
    elif contrast == "treatment":
        X = data[x].replace(TreatmentContrX)
    else:
        return False
    # X.insert(0, "Intercept", 1)

    contrY = {
        "causal": 1,
        "non_causal": 0
    }
    Y = data[y].replace(contrY)

    ## Add an interaction effect
    if interaction == "c1 in group":
        # mapping with nesteed effect: What is the effect of c1 for the SZ and what for the HC group?
        # add columns to X to capture the interaction effect
        X["c1_HC"] = (data["group"] == "HC") * X["c1"]
        X["c1_SZ"] = (data["group"] == "SZ") * X["c1"]
        # remove column that holds primary c1 effect and reorder columns
        X = X.drop(columns=["c1"])
    elif interaction == "group in c1":
        # mapping with nesteed effect: What is the effect of sz for the causal and what for the non_causal group?
        X["group_non_causal"] = (data["c1"] == "non_causal") * X["group"]
        X["group_causal"] = (data["c1"] == "causal") * X["group"]
        # remove column that holds primary sz effect and reorder columns
        X = X.drop(columns=["group"])

    return (X, Y)

def nestPredictors(data:pd.DataFrame, inner:str, outer:str, drop_inner:bool=True) -> Tuple[pd.DataFrame, list]:
    """Nest the inner predictor in the outer predictor.
    Args:
        data (pd.DataFrame): _description_
        inner (str): one of "c1", "group", "time", "angle"
        outer (str): one of "c1" or "group"
    Returns:
        Tuple(pd.DataFrame, list): dataset with two new columns for the nested effect and without the inner predictor. And the list of new columns
    """
    if outer == "group":
        lvls = ("HC", "SZ")
    elif outer == "c1":
        lvls = ("non_causal", "causal")
    else:
        print("ERROR: No valid outer predictor defined.")
        return False

    data2 = data.copy()
    new_cols = []
    for lvl in lvls:
        new_col = inner + "_" + lvl
        new_cols.append(new_col)
        data2[new_col] = (data2[outer] == lvl) * data2[inner]
    if drop_inner:
        data2.drop(columns=[inner], inplace=True)
    data2.replace("", None, inplace=True)

    return data2, new_cols

def applyCustomConstrasts(data:pd.DataFrame, columns:list=["c1", "group"], kind:str="Sum") -> pd.DataFrame:
    data2 = data.copy()

    if kind == "Sum":
        _contrasts = {
            "non_causal": -1,
            "causal": 1,
            "HC": -1,
            "SZ": 1,
            "": 0,
            None: 0,
            np.NaN: 0
        }
    elif kind == "Treament":
        _contrasts = {
            "non_causal": 0,
            "causal": 1,
            "HC": 0,
            "SZ": 1,
            "": 0,
            None: 0,
            np.NaN: 0
        }
    else: 
        print("ERROR: unsupported contrast. Try 'Sum' or 'Treatment'")
        return False
    if data2.isna().any().sum():
        print("WARNING: Some rows contain None values and are coded with 0.")

    data2[columns] = data2[columns].replace(_contrasts)

    return data2

def loadOrSample(model:bmb.Model, trace_file:str, force_sample:bool=False, chains:int=None, draws:int=1000, tune:int=1000) -> az.InferenceData:
    """If a trace for the model already exists, it is loaded. If not, the model is sampled.
    Args:
        model (bmb.Model): bambi model to load or sample
        trace_file (str): path to the trace file. If existing, it is loaded. If not, the trace is stored there.
        force_sample (bool, optional): Force sampling and overwriting an existing trace file. Defaults to False.
        chains (int, optional): Number of chains. If None, the bambi default (4) is used. Defaults to None.
        draws (int, optional): Number of draws. Defaults to 1000.
        tune (int, optional): Number of tuning draws. Defaults to 1000.

    Returns:
        az.InferenceData: _description_
    """
    if os.path.isfile(trace_file) and not force_sample:
        print("Loading trace from file.")
        return az.from_netcdf(trace_file)
    else:
        print("Sampling new trace.")
        model.build()
        # add log-likelihood during sampling to enable arviz model comparison
        # source: https://discourse.pymc.io/t/log-likelihood-not-found-in-inferencedata/8169
        trace = model.fit(chains=chains, draws=draws, tune=tune, idata_kwargs={"log_likelihood": True})
        try:
            trace.to_netcdf(trace_file)
        except:
            print("ERROR when storing trace.")
        return trace
