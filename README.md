# Choice- and trial-history effects on causality perception in Schizophrenia

Here you can find the code underlying the analyses presented in "Choice- and trial-history effects on causality perception in Schizophrenia".

The conda environment used for the original analyses was exported to `environment.yml`.

## Structure

* The scripts `dataLoader.py` and `buttler.py` contain helpful functions for loading and pre-processing data.
* The jupyter notebooks contain the actual analyses.
* The traces sampled for estimating the model parameters (`./traces`) are kept for convenience for any users who which to follow the analyses without the potentially time consuming sampling steps.

## Data

The underlying data is found at the [Open Science Framework](https://osf.io/s6x9g/).

## Licence

MIT (see LICENCE.txt)
