import os
import pandas as pd
from typing import Tuple
import json


with open("./settings.json") as f:
    settings = json.load(f)

causality_experiment_data_file_path = os.path.join(settings["data_path"], settings["causality_data_file"])

# Data loading and pre-processing
# ----------------------
def loadCausalityData() -> Tuple[pd.DataFrame, list]:
    """qol function for loading data

    Returns:
        Tuple[pd.DataFrame, list]: (dataframe, list of trial identifiers)
    """
    df = pd.read_csv(causality_experiment_data_file_path, sep="\t", decimal=",")
    df.index.name = "id"

    # drop NaNs
    df = df.dropna()

    # drop responses with an error
    df = df[df.error_responses == 0]
    df = df.drop(columns=["error_responses"])

    # drop healthy control participants with "seection_matching" = 0 to balance SZ and HC patients
    df.selection_matching = df.selection_matching.replace(" ", "0")
    df = df.astype({
        "selection_matching": int,
    })
    df = df[df["selection_matching"] == 1]
    df = df.drop(columns=["selection_matching"])

    # drop causality_judgements not 0 or 1
    df = df.drop(df[~ ((df.causality_judgement == 0) | (df.causality_judgement == 1)) ].index)

    # reorder columns
    df = df[["subject_id", "trialnumber"] + list(df.columns[1:8]) + ["RT_corrected_for_delay"]]

    # identify trial identifiers
    TRIAL_IDS = ["subject_id", "baseline_stimulation", "stimulation", "group"]

    print(f"Causality data shape: {df.shape}")
    
    return df, TRIAL_IDS

def loadParticipantData() -> pd.DataFrame:
    data_file_path = os.path.join(settings["data_path"], settings["demographics_and_diagnosis_data_file"])
    df = pd.read_csv(data_file_path, index_col="ProbandenID")

    return df


def shiftColumns(df:pd.DataFrame, columns:list, SESSION_IDS:list, shift_by:int=1) -> pd.DataFrame:
    """Shift each column in columns for each session uniquely identified by SESSION_IDS by shift_by trials and add it as a new column to the dataframe

    Args:
        df (pd.DataFrame): _description_
        columns (list): _description_
        SESSION_IDS (list): _description_
        shift_by (int, optional): _description_. Defaults to 1.

    Returns:
        pd.DataFrame: _description_
    """
    # add shifted column value
    def shift_trial(group):
        for column in columns:
            group[column + "1"] = group[column].shift(shift_by)
        return group
    data = df.groupby(SESSION_IDS, group_keys=False).apply(shift_trial)

    data = data.dropna()
    print(f"Shifted Data shape: {data.shape}")
    return data
